﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CryptikLab {

    [ExecuteInEditMode]
    public class Console : MonoBehaviour {

        static Console instance;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        static void SelfInit() {
            GameObject go = new GameObject("Console", typeof(Console));
            go.isStatic = true;
            DontDestroyOnLoad(go);
            instance = go.GetComponent<Console>();
        }

        [SerializeField] GUISkin skin;

        List<int> logFps = new List<int>();

        int curFps, minFps, maxFps, avgFps;

        Texture2D blankTex;

        float targetTimeScale = 1;

        void Awake() {
            instance = this;

            StartCoroutine(CountFrameRate());
        }

        private void Update() {
            if (Input.GetKeyDown(KeyCode.F3))
                targetTimeScale = 1.2f - targetTimeScale;
            if (Mathf.Abs(Time.timeScale - targetTimeScale) > .01f)
                Time.timeScale = Mathf.Lerp(Time.timeScale, targetTimeScale, Time.deltaTime * 10);
        }

        IEnumerator CountFrameRate() {

            int latestFrameCount = 0;

            for (;;) {

                yield return new WaitForSeconds(1);

                curFps = Time.frameCount - latestFrameCount;

                minFps = logFps.Count > 0 ? Mathf.Min(curFps, minFps) : curFps;
                maxFps = Mathf.Max(curFps, maxFps);

                avgFps = Mathf.RoundToInt(Time.frameCount / Time.time);

                logFps.Add(curFps);

                if (logFps.Count > 10) logFps.RemoveAt(0);

                latestFrameCount = Time.frameCount + 1; //@NOTE: skip 1 frame for nothing
            }
        }

        void OnGUI() {

            float msec = curFps > 0 ? 1f / curFps * 1000f : 0;
            string strFps = string.Format("{0} FPS ({1:0.0} ms) from {2}~{3} AVG {4}\n",
                curFps, msec, minFps, maxFps, avgFps);

            Rect label = new Rect(Screen.width / 2f - 128, 2, 512, 64);
            GUI.Label(label, strFps);
        }

        private void OnDestroy() {
            Resources.UnloadUnusedAssets();
            System.GC.Collect();
        }
    }
}