﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CryptikLab {
    public class ObjectPooler : MonoBehaviour {

        public static ObjectPooler instance;

        [Header("Config")]
        [SerializeField]
        string[] resourcesPaths = new string[0];
        [SerializeField]
        Transform[] prefabData;

        List<Transform> pooledObjs = new List<Transform>();

        private void Awake() {

            // Load prefab data
            List<Transform> prefabs = new List<Transform>();

            // Add from child list
            for (int i = 0; i < transform.childCount; i++) {
                Transform child = transform.GetChild(i);
                prefabs.Add(child);
                pooledObjs.Add(child);
                if (child.gameObject.activeSelf)
                    child.gameObject.SetActive(false);
            }
            // Add from resources
            for (int i = 0; i < resourcesPaths.Length; i++) {
                Transform[] trans = Resources.LoadAll<Transform>(resourcesPaths[i]);
                for (int j = 0; j < trans.Length; j++) {
                    if (!prefabs.Contains(trans[j]) && !prefabs.Exists(x => x.name.Equals(trans[j].name)))
                        prefabs.Add(trans[j]);
                }
            }

            prefabData = prefabs.ToArray();

            instance = this;
        }

        bool CheckIsValid(Transform target, string prefabName) {
            return !target.gameObject.activeSelf && target.name.Replace("(Clone)", string.Empty).Equals(prefabName);
        }

        public Transform GetFreeObj(string prefabName) {

            // Try to get a free instance
            Transform freeObj = pooledObjs.Find((x) => CheckIsValid(x, prefabName));

            if (!freeObj) {
                // Create new instance
                Transform prefab = Array.Find(prefabData, (x) => x.name.Equals(prefabName));
                if (prefab) {
                    freeObj = Instantiate(prefab, transform);
                    pooledObjs.Add(freeObj);
                } else {
                    //@TODO: prefab is not found
                }
            }

            return freeObj;
        }

        public T GetFreeObj<T>(string prefabName) where T : MonoBehaviour {
            Transform freeObj = GetFreeObj(prefabName);
            return freeObj ? freeObj.GetComponent<T>() : null;
        }

        public T GetFreeObj<T>() where T : MonoBehaviour {
            return GetFreeObj<T>(prefabData[UnityEngine.Random.Range(0, prefabData.Length)].name);
        }
    }
}