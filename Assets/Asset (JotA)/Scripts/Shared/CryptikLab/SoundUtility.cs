﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundUtility {

    public static List<AudioSource> pooledAudioSources = new List<AudioSource>();
    public static Dictionary<string, AudioClip> loadedClips = new Dictionary<string, AudioClip>();

    /// <summary>
    /// Determine a clip is loaded from Resources depend path name (as a key).
    /// </summary>
    public static bool ClipIsLoaded(string resPath) {
        return loadedClips.ContainsKey(resPath);
    }

    /// <summary>
    /// Get audio clip from loadeds list or load from Resources.
    /// </summary>
    public static AudioClip GetClipOrLoad(string resPath) {

        AudioClip audioClip;
        loadedClips.TryGetValue(resPath, out audioClip);

        if (!audioClip) {
            audioClip = Resources.Load<AudioClip>(resPath);
            if (audioClip)
                loadedClips.Add(resPath, audioClip);
            else
                Debug.Log("<color=#ff0000>Can't load audio clip from resources path </color>" + resPath);
        }

        return audioClip;
    }

    /// <summary>
    /// Unload clip from Resources by path name (as a key).
    /// </summary>
    public static void UnloadClip(string resPath) {
        if (ClipIsLoaded(resPath)) {
            Resources.UnloadAsset(loadedClips[resPath]);
            loadedClips.Remove(resPath);
        }
    }

    public static AudioSource PoolAudioSource(AudioClip clip = null, bool loop = false, float volume = 1, float pitch = 1) {

        AudioSource freeAudioSource = pooledAudioSources.Find(x => !x.isPlaying);

        if (!freeAudioSource) {

            GameObject go = new GameObject("AudioSource", typeof(AudioSource));
            GameObject.DontDestroyOnLoad(go);

            freeAudioSource = go.GetComponent<AudioSource>();

            pooledAudioSources.Add(freeAudioSource);
        }

        freeAudioSource.clip = clip;
        freeAudioSource.loop = loop;
        freeAudioSource.volume = volume;
        freeAudioSource.pitch = pitch;

        return freeAudioSource;
    }
}