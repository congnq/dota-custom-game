﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CryptikLab {
    public class InputUtility : MonoBehaviour {

        //#region Instance
        //static InputUtility instance;

        //[RuntimeInitializeOnLoadMethod]
        //static void SelfInit() {
        //    GameObject go = new GameObject("Input Utility", typeof(InputUtility));
        //    go.isStatic = true;
        //    DontDestroyOnLoad(go);
        //    instance = go.GetComponent<InputUtility>();
        //}
        //#endregion


        #region Services
        const float SMOOTH_ROTATION = 14;
        private static Quaternion smoothGyroRot, smoothAcceRot;

        private static Quaternion GyroToUnity(Quaternion q) {
            return new Quaternion(q.x, q.y, -q.z, -q.w);
        }

        private static float GetNormalAngle(float angle, float left = -90, float right = 90) {

            angle = Mathf.DeltaAngle(0, angle);
            angle = Mathf.Clamp(angle, left, right);

            return angle;
        }

        public static Quaternion GetDeviceRotation() {

            return SystemInfo.supportsGyroscope ? GetGyroscopeRotation() : GetAccelerometerRotation();
        }

        public static Quaternion GetGyroscopeRotation(bool smooth = true, bool around = false) {

            if (SystemInfo.supportsGyroscope) {
                if (!Input.gyro.enabled)
                    Input.gyro.enabled = true;
            } else return Quaternion.identity;

            Quaternion gyro = GyroToUnity(Input.gyro.attitude);
            Quaternion rotation = Quaternion.Euler(Vector3.right * 90) * gyro;

            if (!around) {

                Vector3 eulerAngle = new Vector3(
                    GetNormalAngle(rotation.eulerAngles.x),
                    GetNormalAngle(rotation.eulerAngles.y),
                    GetNormalAngle(rotation.eulerAngles.z));

                float yaw = 1 - Mathf.Abs(eulerAngle.x / 90);
                eulerAngle.z *= yaw;

                //Debug.Log(rotation.eulerAngles + " .... " + eulerAngle);

                Quaternion rotForward = Quaternion.AngleAxis(eulerAngle.z, Vector3.forward);
                Quaternion rotRight = Quaternion.AngleAxis(eulerAngle.x, Vector3.right);

                rotation = rotForward * rotRight;
            }

            if (smooth)
                rotation = smoothGyroRot = Quaternion.Slerp(smoothGyroRot, rotation, Time.deltaTime * SMOOTH_ROTATION);

            return rotation;
        }

        public static Quaternion GetAccelerometerRotation(bool smooth = true) {

            Vector3 acceleration = Input.acceleration.normalized;

            //Debug.Log("Rot back: " + (acceleration.x * 90) + " ; Rot left: " + (acceleration.z * 90));

            Quaternion rotBack = Quaternion.AngleAxis(acceleration.x * 90, Vector3.back);
            Quaternion rotLeft = Quaternion.AngleAxis(acceleration.z * 90, Vector3.left);

            Quaternion rotation = rotBack * rotLeft;

            if (smooth)
                rotation = smoothAcceRot = Quaternion.Slerp(smoothAcceRot, rotation, Time.deltaTime * SMOOTH_ROTATION);

            return rotation;
        }
        #endregion
    }
}