﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CryptikLab {

    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    public class CameraOrbit : MonoBehaviour {

        [SerializeField] Transform targetObject;
        [SerializeField] float distance = 5;
        [SerializeField] Vector3 angleAxes;
        [SerializeField] Vector3 targetOffset;

        [SerializeField] float angleSmoothess = 10, distSmoothess = 5;
        [SerializeField] float minDistance = 1, maxDistance = 20;
        [SerializeField] float minPitch = -10, maxPitch = 80;

        Camera camera;

        Vector3 smoothAngleAxes;
        float smoothDistance;

        Vector3 latestPointerPos;

        public Transform TargetObject {
            get { return targetObject; }
            set {
                targetObject = value;
                OnEnable();
            }
        }
        public Vector3 Offset { get { return targetOffset; } set { targetOffset = value; } }
        public float Pitch { get { return angleAxes.x; } set { angleAxes.x = value; } }
        public float Roll { get { return angleAxes.y; } set { angleAxes.y = value; } }
        public float Yaw { get { return angleAxes.z; } set { angleAxes.z = value; } }
        public float Distance { get { return distance; } set { distance = value; } }

        public RaycastHit PointerHit { get; private set; }
        public bool IsPointerHit { get; private set; }

        public Vector3 NormalizedDirection(float fowardScale, float rightScale) {

            Vector3 normalizedFwd = new Vector3(transform.forward.x, 0, transform.forward.z).normalized;
            Vector3 normalizedRt = new Vector3(transform.right.x, 0, transform.right.z).normalized;

            return normalizedFwd * fowardScale + normalizedRt * rightScale;
        }

        public Vector3 GetPointerDelta(float distOfDepth) {

            Vector3 worldPointer = GetWorldPointer(distOfDepth);
            Vector3 deltaPos = worldPointer - latestPointerPos;
            latestPointerPos = worldPointer;

            return deltaPos;
        }

        public Vector3 GetWorldPointer(float distOfDepth) {
            Vector3 screenPointer = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distOfDepth);
            return camera.ScreenToWorldPoint(screenPointer);
        }

        private void Start() {
            camera = GetComponent<Camera>();
        }

        private void OnEnable() {

            smoothAngleAxes = angleAxes;
            smoothDistance = distance;
        }

        private void Update() {

            // Update pointer hit info
            if (camera) {
                RaycastHit hit;
                Vector3 viewport = new Vector3(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
                IsPointerHit = Physics.Raycast(camera.ViewportPointToRay(viewport), out hit);
                PointerHit = hit;
            }
        }

        private void LateUpdate() {

            if (!camera || !targetObject) return;

            // Clamping angle axes then smoothing
            Pitch = ClampAngle(angleAxes.x, minPitch, maxPitch);
            smoothAngleAxes = Vector3.Lerp(smoothAngleAxes, angleAxes, Time.deltaTime * angleSmoothess);

            // Clamping distance then smoothing
            distance = Mathf.Clamp(distance, minDistance, maxDistance);
            smoothDistance = Mathf.Lerp(smoothDistance, distance, Time.deltaTime * distSmoothess);

            // Calculate position and rotation
            Vector3 targetPoint = targetObject.position + targetOffset;
            Quaternion rotation = Quaternion.Euler(smoothAngleAxes);
            Vector3 negDistance = Vector3.back * smoothDistance;
            Vector3 position = rotation * negDistance + targetPoint;

            // Translate
            //if (Vector3.Distance(transform.position, position) > 0f)
            transform.position = position;

            // Rotate
            //if (Quaternion.Angle(transform.rotation, rotation) > 0f)
            transform.rotation = rotation;
        }

        public static float ClampAngle(float angle, float min, float max) {
            if (angle < -360F) angle += 360F;
            if (angle > 360F) angle -= 360F;
            return Mathf.Clamp(angle, min, max);
        }
    }
}
