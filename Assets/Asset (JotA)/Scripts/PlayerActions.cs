﻿namespace CryptikLab {
    using InControl;
    using UnityEngine;


    public class PlayerActions : PlayerActionSet {

        public PlayerAction Fire;
        public PlayerAction Melee;
        public PlayerAction Jump;
        public PlayerAction Uses;

        public PlayerTwoAxisAction Move;
        public PlayerTwoAxisAction Look;

        PlayerAction moveLeft;
        PlayerAction moveRight;
        PlayerAction moveUp;
        PlayerAction moveDown;

        PlayerAction lookLeft;
        PlayerAction lookRight;
        PlayerAction lookUp;
        PlayerAction lookDown;

        public PlayerActions() {

            Fire = CreatePlayerAction("Fire");
            Melee = CreatePlayerAction("Melee");
            Jump = CreatePlayerAction("Jump");
            Uses = CreatePlayerAction("Uses");

            moveLeft = CreatePlayerAction("Move Left");
            moveRight = CreatePlayerAction("Move Right");
            moveUp = CreatePlayerAction("Move Up");
            moveDown = CreatePlayerAction("Move Down");

            Move = CreateTwoAxisPlayerAction(moveLeft, moveRight, moveDown, moveUp);

            lookLeft = CreatePlayerAction("Look Left");
            lookRight = CreatePlayerAction("Look Right");
            lookUp = CreatePlayerAction("Look Up");
            lookDown = CreatePlayerAction("Look Down");

            Look = CreateTwoAxisPlayerAction(lookLeft, lookRight, lookUp, lookDown);

            Jump.AddDefaultBinding(InputControlType.Action1);
            Uses.AddDefaultBinding(InputControlType.Action2);
            Melee.AddDefaultBinding(InputControlType.Action3);
            Fire.AddDefaultBinding(InputControlType.Action4);

            moveLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
            moveRight.AddDefaultBinding(InputControlType.LeftStickRight);
            moveUp.AddDefaultBinding(InputControlType.LeftStickUp);
            moveDown.AddDefaultBinding(InputControlType.LeftStickDown);

            lookLeft.AddDefaultBinding(InputControlType.RightStickLeft);
            lookRight.AddDefaultBinding(InputControlType.RightStickRight);
            lookUp.AddDefaultBinding(InputControlType.RightStickUp);
            lookDown.AddDefaultBinding(InputControlType.RightStickDown);

#if UNITY_EDITOR || UNITY_STANDALONE
            //Fire.AddDefaultBinding(Mouse.LeftButton);
            Jump.AddDefaultBinding(Key.Space);

            //lookLeft.AddDefaultBinding(Mouse.NegativeX);
            //lookRight.AddDefaultBinding(Mouse.PositiveX);
            //lookUp.AddDefaultBinding(Mouse.PositiveY);
            //lookDown.AddDefaultBinding(Mouse.NegativeY);

            moveUp.AddDefaultBinding(Key.UpArrow);
            moveDown.AddDefaultBinding(Key.DownArrow);
            moveLeft.AddDefaultBinding(Key.LeftArrow);
            moveRight.AddDefaultBinding(Key.RightArrow);

            moveLeft.AddDefaultBinding(Key.A);
            moveRight.AddDefaultBinding(Key.D);
            moveUp.AddDefaultBinding(Key.W);
            moveDown.AddDefaultBinding(Key.S);
#endif

            ListenOptions.IncludeUnknownControllers = true;
            ListenOptions.MaxAllowedBindings = 4;
            //playerActions.ListenOptions.MaxAllowedBindingsPerType = 1;
            //playerActions.ListenOptions.AllowDuplicateBindingsPerSet = true;
            ListenOptions.UnsetDuplicateBindingsOnSet = true;
            //playerActions.ListenOptions.IncludeMouseButtons = true;
            //playerActions.ListenOptions.IncludeModifiersAsFirstClassKeys = true;
            //playerActions.ListenOptions.IncludeMouseButtons = true;
            //playerActions.ListenOptions.IncludeMouseScrollWheel = true;

            ListenOptions.OnBindingFound = (action, binding) => {
                if (binding == new KeyBindingSource(Key.Escape)) {
                    action.StopListeningForBinding();
                    return false;
                }
                return true;
            };

            ListenOptions.OnBindingAdded += (action, binding) => {
                Debug.Log("Binding added... " + binding.DeviceName + ": " + binding.Name);
            };

            ListenOptions.OnBindingRejected += (action, binding, reason) => {
                Debug.Log("Binding rejected... " + reason);
            };
        }
    }
}
