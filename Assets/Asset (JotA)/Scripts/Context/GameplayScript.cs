﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CryptikLab {
    public class GameplayScript : MonoBehaviour {

        [SerializeField] CameraOrbit cameraOrbit;

        [SerializeField] HeroBehaviour[] controlUnits;
        HeroBehaviour controlUnit;

        PlayerActions playerActions;

        bool running = true;

        private void Start() {

            controlUnit = controlUnits[0];

            playerActions = new PlayerActions();

#if UNITY_EDITOR
            //TouchManager.ControlsEnabled = false;
#endif
        }

        private void Update() {

            if (controlUnit && cameraOrbit.TargetObject != controlUnit.transform)
                cameraOrbit.TargetObject = controlUnit.transform;

#if UNITY_EDITOR || UNITY_STANDALONE
            if (Input.GetKeyDown(KeyCode.Tab)) {
                int index = controlUnit ? (int)Mathf.Repeat(
                    System.Array.IndexOf(controlUnits, controlUnit) + 1, controlUnits.Length) : 0;
                controlUnit = controlUnits[index];
            }
            if (Input.GetKeyDown(KeyCode.F))
                controlUnit.locomotion.AnimationSpeed = 1 - controlUnit.locomotion.AnimationSpeed;
            if (Input.GetMouseButtonDown(1) && cameraOrbit.IsPointerHit) {
                controlUnit.MoveToPosition(cameraOrbit.PointerHit.point);
            }
            if (Input.GetMouseButtonDown(2) && cameraOrbit.IsPointerHit) {
                controlUnit.CastSkillIndex(1, cameraOrbit.PointerHit.point);
            }
#endif

            // Handle player interaction
            if (playerActions.Move.IsPressed) {

                Vector2 stickVal = Vector2.ClampMagnitude(playerActions.Move.Value, 1);

                Vector3 deltaPos = cameraOrbit.NormalizedDirection(stickVal.y, stickVal.x);

                if (Input.GetKeyDown(KeyCode.LeftShift)) running = !running;

                if (Input.GetKey(KeyCode.E))
                    deltaPos *= 4;
                else if (Input.GetKey(KeyCode.Q))
                    deltaPos *= 3;
                else if (running)
                    deltaPos *= 2;

                controlUnit.locomotion.InputDeltaPos = deltaPos;
                controlUnit.locomotion.InputLookDir = deltaPos;
            }

            if (playerActions.Jump.WasPressed) {
                controlUnit.locomotion.JumpToPoint(1, Vector3.zero);
                //int index = controlUnit ? (int)Mathf.Repeat(
                //    System.Array.IndexOf(controlUnits, controlUnit) + 1, controlUnits.Length) : 0;
                //controlUnit = controlUnits[index];
            }

            bool attacking = playerActions.Uses.IsPressed;
            if (attacking) {
                //controlUnit.CastAbility1(controlUnit.transform.position + controlUnit.transform.forward * 20);
            }
            // Handle camera interaction
            else if (playerActions.Look.IsPressed) {
                cameraOrbit.Pitch += playerActions.Look.Y * 5;
                cameraOrbit.Roll += playerActions.Look.X * 5;

                bool camTypedTopDown = cameraOrbit.Pitch > 70;
                cameraOrbit.Distance = camTypedTopDown ? 10 : (cameraOrbit.Pitch > 50 ? 8 : 5);
            }
        }
    }
}