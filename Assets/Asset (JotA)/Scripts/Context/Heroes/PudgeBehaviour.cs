﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PudgeBehaviour : HeroBehaviour {

    [SerializeField] PudgeWeapon weapon;

    [SerializeField] AnimationCurve animSpeedCurve = AnimationCurve.Linear(0, 1, .1f, 1);

    private void Start() {

        weapon.OnHookHit = (Collider victim) => {

        };

        weapon.OnHookPull = (Collider victim, Vector3 hook) => {

            HeroBehaviour hero = victim.GetComponent<HeroBehaviour>();
            if (hero) {
                hook.y = hero.transform.position.y;
                hero.transform.position = Vector3.Lerp(hero.transform.position, hook, Time.deltaTime * 10);
                //hero.locomotion.InputDeltaPos = hook - hero.transform.position;
            }
        };

        weapon.OnHookDone = (Collider victim) => {
            OnActionEnd(3);
        };

        weapon.OnHookFly = (float timing) => {
            locomotion.AnimationSpeed = animSpeedCurve.Evaluate(timing);
        };
    }

    private void FixedUpdate() {

        // Syncs mine hook
        weapon.SyncsWithHand(locomotion.RightHand);
    }

    protected override void OnActionBegin(int index) {
        weapon.Refresh(); //absolity
    }

    protected override void OnActionEmit(int index) {
        weapon.ThrowHook(transform.position + locomotion.TargetLookDir.normalized * 15 + Vector3.up);
    }

    protected override void OnActionEnd(int index) {
        locomotion.AnimationSpeed = 1;
    }
}
