﻿using Pathfinding;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provide common services help finding path
/// </summary>
public class Pathfinder : MonoBehaviour {

    struct Request {

        public Vector3 start;
        public Vector3 end;
        public Action<Path> callback;
        public Request(Vector3 start, Vector3 end, Action<Path> callback) {
            this.start = start;
            this.end = end;
            this.callback = callback;
        }
    }

    static AstarPath astar;
    static Seeker seeker;

    static List<Request> reqs = new List<Request>();

    bool busing;

    public static void FindPath(Vector3 start, Vector3 end, Action<Path> callback) {
        reqs.Add(new Request(start, end, callback));
    }

    void Awake() {

        astar = GetComponentInChildren<AstarPath>();
        seeker = GetComponentInChildren<Seeker>();

        seeker.drawGizmos = false;
    }

    private void Update() {

        if (reqs.Count > 0) {
            if (!busing) {
                busing = true;
                Request req = reqs[0];
                //Debug.Log("Find path from " + req.start + " to " + req.end);
                seeker.StartPath(req.start, req.end, (Path p) => {
                    if (req.callback != null)
                        req.callback.Invoke(p);
                    reqs.RemoveAt(0);
                    busing = false;
                    //Debug.Log("Result: " + p.path.Count + " step");
                });
            }
        }
    }
}
