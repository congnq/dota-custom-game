﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityStateBehaviour : StateMachineBehaviour {

    public float[] eventTriggers;
    public Action<int> onThrowEvent;
    public Action onStateEnter;
    public Action onStateExit;

    int triggedEvents;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (onStateEnter != null)
            onStateEnter.Invoke();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        if (eventTriggers != null && triggedEvents < eventTriggers.Length) {
            // Count trigged event and invoke call back
            if (stateInfo.normalizedTime > eventTriggers[triggedEvents]) {
                if (onThrowEvent != null)
                    onThrowEvent.Invoke(triggedEvents);
                triggedEvents++;
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        // Reset trigged events counter if needed
        if (triggedEvents > 0)
            triggedEvents = 0;

        // Invoke callback event
        if (onStateExit != null)
            onStateExit.Invoke();

        // Return action in progress value
        animator.SetInteger("Action In Progress", -1);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
