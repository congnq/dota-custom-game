﻿using RootMotion.FinalIK;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class HeroLocomotion : MonoBehaviour {

    CharacterController controller;
    Animator animator;
    BipedIK ik;

    [HideInInspector]
    public AnimatorOverrideController animatorOverrideController;

    [SerializeField] AnimationClip altIdleClip;
    [SerializeField] AnimationClip altAttackClip;
    [SerializeField] float[] timeAttackTriggers;

    float jumpNextFrame;

    public Vector3 InputDeltaPos { get; set; }
    public Vector3 DeltaPosition { get; private set; }
    public Vector3 InputLookDir { get; set; }
    public Vector3 TargetLookDir { get; set; }
    public Vector3 TargetLookAt { get; set; }
    public bool IsGrounded { get; private set; }

    public Transform LeftHand { get; private set; }
    public Transform RightHand { get; private set; }

    public int ActionInProg {
        get { return animator.GetInteger("Action In Progress"); }
        set {
            if (ActionInProg <= 0)
                animator.SetInteger("Action In Progress", value);
        }
    }

    public float AnimationSpeed {
        get { return animator.speed; }
        set { animator.speed = value; }
    }

    public void ResetTargetLookAt() {
        TargetLookAt = Vector3.zero;
    }

    public void ResetTargetLookDir() {
        TargetLookDir = Vector3.zero;
    }

    private void Awake() {

        controller = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
        ik = GetComponentInChildren<BipedIK>();

        LeftHand = animator.GetBoneTransform(HumanBodyBones.LeftHand);
        RightHand = animator.GetBoneTransform(HumanBodyBones.RightHand);

        animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = animatorOverrideController;

        animatorOverrideController["Idle"] = altIdleClip;
        animatorOverrideController["Attack"] = altAttackClip;

        //// Temporary proc ability event triggers
        //if (actionTriggers != null) {
        //    AbilityStateBehaviour[] abiSBs = animator.GetBehaviours<AbilityStateBehaviour>();

        //    for (int i = 0; i < abiSBs.Length; i++) {
        //        AbilityStateBehaviour abiSB = abiSBs[i];

        //        if (i < actionTriggers.Length)
        //            abiSB.eventTriggers = new float[] { actionTriggers[i] };

        //        abiSB.onThrowEvent = (int index) => {
        //            gameObject.SendMessage("OnActionEmit", ActionInProg);
        //        };
        //        abiSBs[i].onStateEnter = () => {
        //            gameObject.SendMessage("OnActionBegin", ActionInProg);
        //        };
        //        abiSBs[i].onStateExit = () => {
        //            gameObject.SendMessage("OnActionEnd", ActionInProg);
        //            ResetTargetLookDir();
        //            ResetTargetLookAt();
        //        };
        //    }
        //}

        // Default values
        animator.applyRootMotion = false;
        controller.skinWidth = .01f;
        controller.stepOffset = .5f;
        controller.slopeLimit = 30;

        if (ik)
            ik.SetLookAtWeight(1, .3f, 1, 0, .7f, .7f, .5f);
    }

    private void Update() {

        HandleAnimator();

        HandleDeltaPos();
        HandleLooking();

        HandleController();

        HandleBipedIK();
    }

    void HandleBipedIK() {

        if (!ik) return;

        // Look at solver
        Vector3 lookAtPos = TargetLookAt.magnitude > 0 ? TargetLookAt :
            transform.position + transform.forward * 10 + transform.up * (controller.height - .2f);
        ik.SetLookAtPosition(Vector3.Lerp(ik.solvers.lookAt.GetIKPosition(),
            lookAtPos, 30 * Time.deltaTime));
    }

    void HandleLooking() {

        // Remove y axis
        InputLookDir = new Vector3(InputLookDir.x, 0, InputLookDir.z);
        TargetLookDir = new Vector3(TargetLookDir.x, 0, TargetLookDir.z);

        // Handle rotation depend input look delta
        bool hasTargetLook = TargetLookDir.magnitude > 0;
        bool hasInputLook = InputLookDir.magnitude > .5f;

        if (hasTargetLook || hasInputLook) {
            // Define forward vector priority for target look direction
            Vector3 forward = hasTargetLook ? TargetLookDir : InputLookDir;
            Quaternion rotation = Quaternion.LookRotation(forward);

            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 8);
        }

        // Reset looking trigger vairants
        InputLookDir = Vector3.zero;
    }

    void HandleDeltaPos() {

        float smoothSpeed = InputDeltaPos.magnitude > 0 ? 10 : 5;

        DeltaPosition = Vector3.MoveTowards(DeltaPosition, InputDeltaPos, Time.deltaTime * smoothSpeed);

        // Reset trigger for next frame
        InputDeltaPos = Vector3.zero;
    }

    void HandleController() {

        // Determinate is grounded
        IsGrounded = Physics.Raycast(transform.position, Vector3.down, controller.skinWidth * 2);

        if (jumpNextFrame > 0) {
            // Calculate jump
            jumpNextFrame = Mathf.Sqrt(2 * jumpNextFrame * Mathf.Abs(Physics.gravity.y));
        } else {
            // Apply gravity
            if (controller.isGrounded)
                jumpNextFrame = Mathf.Min(0, controller.velocity.y);
            else
                jumpNextFrame = Mathf.Max(-20, controller.velocity.y + Physics.gravity.y * Time.deltaTime);
        }

        // Move the controller and reset motion
        Vector3 animDeltaPos = animator.deltaPosition;

        if (!IsGrounded) {
            animDeltaPos.x *= 2 / 3f;
            animDeltaPos.z *= 2 / 3f;
        }

        controller.Move(new Vector3(animDeltaPos.x, jumpNextFrame * Time.deltaTime, animDeltaPos.z));

        // Reset value for next frame
        jumpNextFrame = 0;
    }


    void HandleAnimator() {

        // Recalculate delta pos depend current rotation
        if (animator.speed > 0) {
            Vector3 deltaLocalPos =
                Quaternion.AngleAxis(-transform.eulerAngles.y, Vector3.up) * DeltaPosition;
            float speedMagnitude = DeltaPosition.magnitude;
            if (InputDeltaPos.magnitude > 0) speedMagnitude = Mathf.Max(.1f, speedMagnitude);

            animator.SetFloat("Speed Magnitude", speedMagnitude);

            animator.SetFloat("Forward Speed", deltaLocalPos.z);
            animator.SetFloat("Strafe Speed", deltaLocalPos.x);
        }
    }

    public void JumpToPoint(float height, Vector3 target) {
        // Set trigger variant
        jumpNextFrame = height;
    }
}
