﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PudgeWeapon : MonoBehaviour {

    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    class Hook : MonoBehaviour {

        PudgeWeapon weapon;
        Rigidbody rigid;
        Collider coll;

        Vector3 throwed;
        bool isReturn;
        Collider hitColl;

        private void Awake() {
            // References
            weapon = GetComponentInParent<PudgeWeapon>();
            rigid = GetComponent<Rigidbody>();
            coll = GetComponent<Collider>();
            // Default values
            coll.isTrigger = true;
            rigid.isKinematic = true;
            rigid.centerOfMass = Vector3.forward * .75f;
        }
        private void FixedUpdate() {

            Vector3 targetPos = weapon.hookHandPosWorld;
            Quaternion targetRot = weapon.hookHandRotWorld;
            float moveSpeed = 150;
            float rotateSpeed = 50;

            if (throwed.magnitude > 0) {

                float maxLength = Vector3.Distance(weapon.hookHandPosWorld, throwed);
                float curLength = Vector3.Distance(weapon.hookHandPosWorld, rigid.position);
                float normLength = curLength / maxLength;

                if (weapon.OnHookFly != null && !float.IsNaN(normLength)) {
                    float timing = isReturn ? .5f + (1 - normLength) / 2f : normLength / 2f;
                    weapon.OnHookFly.Invoke(timing);
                }

                if (isReturn) {

                    if (normLength > .1f)
                        targetRot = Quaternion.LookRotation(rigid.position - weapon.hookHandPosWorld);

                    if (curLength < Time.maximumDeltaTime) {

                        Rest();

                        if (weapon.OnHookDone != null)
                            weapon.OnHookDone.Invoke(hitColl);

                    } else if (hitColl && weapon.OnHookPull != null)
                        weapon.OnHookPull.Invoke(hitColl, rigid.position);

                } else {

                    targetPos = throwed;

                    Vector3 forward = normLength < .9f ? throwed - rigid.position :
                        forward = rigid.position - weapon.hookHandPosWorld;
                    targetRot = Quaternion.LookRotation(forward);

                    if (normLength >= 1f) {
                        isReturn = true;
                    }
                }

                moveSpeed /= 10;
                rotateSpeed /= 10;

                //Vector3 hookPvtPos = hook.transform.TransformPoint(new Vector3(0, .007f, -.24f));
                //Debug.DrawLine(rigid.position, weapon.hookHandPosWorld);
                //Debug.DrawLine(rigid.position, throwed, Color.gray);
            }

            rigid.position = Vector3.MoveTowards(
                rigid.position, targetPos, Time.deltaTime * moveSpeed);
            rigid.rotation = Quaternion.Slerp(
                rigid.rotation, targetRot, Time.deltaTime * rotateSpeed);
        }
        private void OnTriggerEnter(Collider other) {

            if (other.name.Equals("Hero-Pudge")) return;

            if (!hitColl && !isReturn) {

                isReturn = true;
                hitColl = other;
                throwed = rigid.position;

                if (weapon.OnHookHit != null)
                    weapon.OnHookHit.Invoke(hitColl);

            }
        }
        public void Rest() {
            isReturn = false;
            hitColl = null;
            throwed = Vector3.zero;
            coll.enabled = false;
        }
        public void Throw(Vector3 target) {
            Rest();
            throwed = target;
            coll.enabled = true;
        }
    }

    Hook hook;

    [SerializeField] Vector3 hookHandPosLocal;
    [SerializeField] Vector3 hookHandEulerLocal;

    Vector3 hookHandPosWorld;
    Quaternion hookHandRotWorld;

    public Action<Collider> OnHookHit { get; set; }
    public Action<Collider, Vector3> OnHookPull { get; set; }
    public Action<Collider> OnHookDone { get; set; }
    public Action<float> OnHookFly { get; set; }

    public void SyncsWithHand(Transform hand) {

        hookHandPosWorld = hand.TransformPoint(hookHandPosLocal);
        hookHandRotWorld = hand.rotation * Quaternion.Euler(hookHandEulerLocal);
    }

    void Start() {

        hook = transform.GetChild(0).gameObject.AddComponent<Hook>();
    }

    public void Refresh() {
        hook.Rest();
    }

    public void ThrowHook(Vector3 target) {
        hook.Throw(target);
    }
}
