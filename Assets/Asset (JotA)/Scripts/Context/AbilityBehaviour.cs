﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityBehaviour : MonoBehaviour {

    public AnimationClip castingClip;
    [Range(0, 1)] float[] eventTriggers;

    public void Cast(Vector3 target) {
        //Debug.Log("Cast into " + target);
        //StartCoroutine(DoCast());
    }
}
