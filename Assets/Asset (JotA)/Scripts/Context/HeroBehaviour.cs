﻿using Pathfinding;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HeroLocomotion))]
public class HeroBehaviour : MonoBehaviour {

    [HideInInspector]
    public HeroLocomotion locomotion;

    public ProjectileBehaviour projectile;
    public AbilityBehaviour[] abilities;

    Path currentPath;
    Vector3 latestMoveDir;
    Action pathCallback;

    public void LoadAllAttacheds() {

        for (int i = 0; i < abilities.Length; i++) {
            locomotion.animatorOverrideController["Cast ability " + (i + 1)] =
                abilities[i].castingClip;
        }
    }

    protected virtual void Awake() {
        // Neccessary components
        locomotion = GetComponent<HeroLocomotion>();
    }

    protected virtual void Start() {
        // Load all attached refs
        LoadAllAttacheds();
    }

    protected virtual void Update() {

        if (currentPath != null)
            UpdateMovingPath();
    }

    private void OnDisable() {
        ReleasePathIfNeeded();
    }

    protected virtual void OnActionBegin(int index) { }
    protected virtual void OnActionEmit(int index) { }
    protected virtual void OnActionEnd(int index) {
        locomotion.ResetTargetLookDir();
    }

    void ReleasePathIfNeeded() {
        if (currentPath != null) {
            currentPath.Release(this);
            currentPath = null;
            latestMoveDir = Vector3.zero;
            locomotion.TargetLookDir = Vector3.zero;
        }
    }

    void UpdateMovingPath(bool updateLocomotion = true, float magnitudeThreshold = .1f) {

        Vector3 shortTarget = currentPath.vectorPath[0];
        shortTarget.y = transform.position.y; // sync y position
        Vector3 moveDir = shortTarget - transform.position;

        // Caching latest move dir
        if (moveDir.magnitude > 0) latestMoveDir = moveDir;

        if (updateLocomotion) {
            // Update locomotion input
            locomotion.TargetLookDir = latestMoveDir;
            locomotion.InputDeltaPos = (currentPath.vectorPath.Count > 1 ?
                latestMoveDir.normalized : Vector3.ClampMagnitude(latestMoveDir, 1)) * 2;
            // Draw debug lines
            Debug.DrawLine(transform.position, currentPath.vectorPath[0], Color.yellow);
            for (int i = 0; i < currentPath.vectorPath.Count - 1; i++)
                Debug.DrawLine(currentPath.vectorPath[i], currentPath.vectorPath[i + 1], Color.green);
        }

        if (latestMoveDir.magnitude < magnitudeThreshold) {
            currentPath.vectorPath.RemoveAt(0);
            if (currentPath.vectorPath.Count <= 0) {
                // On move by path completed
                if (pathCallback != null)
                    pathCallback.Invoke();

                ReleasePathIfNeeded();
            }
        }
    }

    public void MoveToPosition(Vector3 destination, Action callback = null) {

        Pathfinder.FindPath(transform.position, destination, (Path p) => {

            if (p.error) {
                //@TODO:
            } else {

                ReleasePathIfNeeded();
                currentPath = p;
                currentPath.Claim(this);
                pathCallback = callback;

                UpdateMovingPath(false, .5f);
            }
        });
    }

    public void AttackForward() {
        locomotion.ActionInProg = 0;
    }

    public void CastSkillIndex(int index, Vector3 target) {
        if (index < 1 || index > abilities.Length) {
            if (abilities.Length > 0)
                Debug.Log("Ability index need in range of " + 1 + " ~ " + abilities.Length);
            else
                Debug.Log("No ability found.");
            return;
        }
        if (locomotion.ActionInProg != index) {
            locomotion.ActionInProg = index;
            locomotion.TargetLookDir = target - transform.position;
            abilities[index - 1].Cast(target);
        }
    }
}
